(define-library (html-writer)
  (export write-html write-html* html->string)
  (import (scheme base)
          (scheme case-lambda)
          (scheme cxr)
          (scheme write))

  (cond-expand
   ;; prefer index-based string manipulation SRFIs over cursor-based ones
   ;; std prefixes are for Gerbil, which as of Feb 2021 only supports SRFI 13, but may support/influence others. when there's a standard library lookup SRFI, the std prefixes can be deleted
   ((library (srfi 152))     (import (srfi 152)))
   ((library (std srfi 152)) (import (std srfi 152)))
   ((library (srfi 13))      (import (srfi 13)))
   ((library (std srfi 13))  (import (std srfi 13)))
   ((library (srfi 130))     (import (srfi 130)))
   ((library (std srfi 130)) (import (std srfi 130))))

  (include "html-writer.scm"))
