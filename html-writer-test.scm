;; to test in Chibi Scheme:
;; (import (scheme small)
;;         (scheme case-lambda)
;;         (srfi 130))

;; to test in Gerbil Scheme:
;; (import (std srfi 13))

(include "html-writer.scm")

(define passed 0)
(define failed 0)
(define (test-writer description input expected-output)
  (let ((actual-output (html->string input)))
    (cond ((equal? expected-output actual-output)
           (display "PASS: ")
           (display description)
           (newline)
           (set! passed (+ 1 passed)))
          (else
           (display "FAIL: ")
           (display description)
           (newline)
           (display "  actual-output ")
           (write actual-output)
           (newline)
           (set! failed (+ 1 failed))))))

(test-writer "basic HTML fragment serializes correctly"
             '(p "hello world")
             "<p>hello world</p>")

(test-writer "HTML with attributes serializes correctly"
             '(p (@ (class "example")) "hello")
             "<p class=\"example\">hello</p>")

(test-writer "empty (void) elements don't get closing tags"
             '(br)
             "<br>")

(test-writer "empty (non-void) elements get closing tags"
             '(div)
             "<div></div>")

(test-writer "HTML with XHTML/MathML/SVG namespaces (as from a strictly-compliant parser) is serialized correctly"
             '(http://www.w3.org/1999/xhtml:html
               (http://www.w3.org/1999/xhtml:body
                (http://www.w3.org/2000/svg:svg)
                (http://www.w3.org/1998/Math/MathML:math)))
             "<html><body><svg></svg><math></math></body></html>")

(test-writer "HTML with named namespaces is serialized correctly"
             '(*top* (@ (*namespaces* (h "http://www.w3.org/1999/xhtml")))
                     (h:p "hello"))
             "<p>hello</p>")

(test-writer "escaping in non-RAWDATA tags"
             '(p "\"<p>\" &c.")
             "<p>\"&lt;p&gt;\" &amp;c.</p>")

(test-writer "no escaping in RAWDATA tags"
             '(script "let paragraphTag = \"<p>\";")
             "<script>let paragraphTag = \"<p>\";</script>")

(test-writer "escaping in attributes"
             '(img (@ (src "image.jpg") (alt "\"<p>\" &c.")))
             "<img src=\"image.jpg\" alt=\"&quot;<p>&quot; &amp;c.\">")

(test-writer "HTML DOCTYPE is serialized correctly"
             '(*top*
               (*doctype* "html")
               (html))
             "<!DOCTYPE html><html></html>")

(test-writer "HTML comment is serialized correctly"
             '(p "hello" (*comment* " world "))
             "<p>hello<!-- world --></p>")

(test-writer "HTML PI is serialized correctly"
             '(p "hello" (*pi* "xml test?"))
             "<p>hello<?xml test?></p>")

(test-writer "namespaces are written as xmlns"
             '(div (@ (*namespaces* (foo "http://example.org/"))))
             "<div xmlns:foo=\"http://example.org/\"></div>")

(test-writer "elements in foreign namespaces are correctly serialized"
             '(div (@ (*namespaces* (foo "http://example.org/")))
                   (foo:bar))
             "<div xmlns:foo=\"http://example.org/\"><foo:bar></foo:bar></div>")

(test-writer "HTML with XLink attribute from named namespace not called xlink gets serialized correctly"
             '(*top*
               (@ (*namespaces* (foo "http://www.w3.org/1999/xlink")))
               (span (@ (foo:href "#")) "click here"))
             "<span xlink:href=\"#\">click here</span>")

(test-writer "HTML with XLink attribute from anonymous namespace gets serialized correctly"
             '(span (@ (http://www.w3.org/1999/xlink:href "#")) "click here")
             "<span xlink:href=\"#\">click here</span>")

(test-writer "complete document renders correctly"
             '(*top*
               (*doctype* "html")
               (html (@ (lang "en"))
                     (meta (@ (charset "utf-8")))
                     (title "Example HTML document in SXML")
                     (body
                      (h1 "An example document")
                      (img (@ (src "success.png") (alt "Success!")))
                      (p "The example document rendered properly!"))))
             "<!DOCTYPE html><html lang=\"en\"><meta charset=\"utf-8\"><title>Example HTML document in SXML</title><body><h1>An example document</h1><img src=\"success.png\" alt=\"Success!\"><p>The example document rendered properly!</p></body></html>")

(display (number->string passed))
(display " passed, ")
(display (number->string failed))
(display " failed.")
(newline)

