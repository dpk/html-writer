(define (element? node)
  (and (pair? node)
       (symbol? (car node))
       (not (magic-name? (car node)))))

(define element-name car)
(define (has-attributes? node)
  (and (pair? node)            ;; isn't e.g. a text node
       (symbol? (car node))    ;; is a valid SXML node
       (pair? (cdr node))      ;; might have attributes
       (pair? (cadr node))     ;; ditto
       (eq? '@ (caadr node)))) ;; has attributes
(define (attributes node)
  (if (has-attributes? node)
        (cdadr node)
        '()))
(define attribute-name car)
(define attribute-value cadr)
(define (children node)
  (if (has-attributes? node)
        (cddr node)
        (cdr node)))

(define (magic-name? name) (eq? (string-ref (symbol->string name) 0) #\*))
(define (magic-node? node types) (and (pair? node) (memq (car node) types)))
(define (comment? node) (magic-node? node '(*comment* *COMMENT*)))
(define (pi? node) (magic-node? node '(*pi* *PI*)))
(define (doctype? node) (magic-node? node '(*doctype* *DOCTYPE*)))
(define (top? node) (magic-node? node '(*top* *TOP*)))

(define well-known-namespaces '(
  "http://www.w3.org/1999/xhtml"
  "http://www.w3.org/1998/Math/MathML"
  "http://www.w3.org/2000/svg"
))

(define void-elements '(area base br col embed hr img input link meta param source track wbr ;; according to the spec
  basefont bgsound frame keygen)) ;; only for the purposes of this algorithm
(define (serializes-as-void? element-name) (pair? (memq element-name void-elements)))

(define rawtext-elements '(style script xmp iframe noembed noframes plaintext)) ;; we assume scripting is disabled
(define (rawtext-element? element-name) (pair? (memq element-name rawtext-elements)))

(define (in-well-known-namespace? qname ns)
  (let-values (((ns-name local-name) (qname-parts qname)))
    (or (not ns-name)
        (let ((ns-url (ns-name->ns-url (string->symbol ns-name) ns)))
          (or (member ns-name well-known-namespaces)
              (and ns-url (member ns-url well-known-namespaces)))))))

(cond-expand
 ((or (library (srfi 13))
      (library (std srfi 13))
      (library (srfi 152))
      (library (std srfi 152)))
  (define (qname-parts name)
    (let* ((strname (symbol->string name))
           (colon-idx (string-index-right strname (lambda (c) (eq? c #\:)))))
      (if colon-idx
          (values (string-copy strname 0 colon-idx)
                  (string->symbol (string-copy strname (+ 1 colon-idx))))
          (values #f name)))))
 ((library (srfi 130))
  (define (qname-parts name)
    (let* ((strname (symbol->string name))
           (colon-idx (string-index-right strname #\:)))
      (if (and (= (string-cursor->index strname colon-idx) 0)
               (not (char=? (string-ref strname 0) #\:)))
          (values #f name)
          (values (string-copy/cursors strname 0 (string-cursor-prev strname colon-idx))
                  (string->symbol (string-copy/cursors strname colon-idx))))))))

(define (local-name name)
  (let-values (((ns-name local-name) (qname-parts name)))
    local-name))

(define (ns-name->ns-url name ns)
  (let ((ns-binding (assv name ns)))
    (if ns-binding
      (cadr ns-binding)
      #f)))

(define (extract-namespaces node)
  (let* ((attrs (attributes node))
         (nsattr (or (assq '*namespaces* attrs)
                     (assq '*NAMESPACES* attrs))))
      (if (pair? nsattr) 
        (cdr nsattr)
        '())))

(define write-html
  (case-lambda
    ((node) (write-html node (current-output-port)))
    ((node port) (write-html* node '() #f port))))

(define (html->string node)
  (let ((port (open-output-string)))
    (write-html node port)
    (get-output-string port)))

(define (write-html* node ns parent-node port)
  (let ((ns* (append (extract-namespaces node) ns)))
    (cond ((element? node) (write-html-element node ns* port))
          ((string? node)
            (if (and (element? parent-node) (rawtext-element? (element-name parent-node)))
                  (display node port)
                  (write-html-escaped-string node port #f)))
          ((comment? node) (write-html-comment node port))
          ((pi? node) (write-html-pi node port))
          ((doctype? node) (write-html-doctype node port))
          ((top? node)
            (for-each (lambda (n) (write-html* n ns* node port))
                      (children node)))
          (else (error "not a valid HTML node:" node)))))

(define (write-html-element node ns port)
  ;; this does NOT correctly handle SXML unnamed namespaces properly, however rare it is that they actually appear in HTML!!
  ;; 'well-known' namespaces (and thus round-tripping from HTML parsers that insist on putting everything in its proper namespaces) will work because HTML says they don't get a qname under any circumstances. but if you try to serialize, say, '(http://example.org/ns:example "this is broken") you'll get the invalid <http://example.org/ns:example>this is broken</...etc...>
  ;; maybe the right thing to do here is just to error out when we see a QName from an unknown namespace
  (let ((tagname
          (if (in-well-known-namespace? (element-name node) ns)
                (symbol->string (local-name (element-name node)))
                (symbol->string (element-name node))))) ;; the problem mentioned above is here
    (display #\< port)
    (display tagname port)
    (for-each (lambda (a) (write-html-element-attribute a ns port))
              (attributes node))
    (display #\> port)
    (if (serializes-as-void? (element-name node))
      (display "" port) ;; no-op
      (begin
        (for-each (lambda (c) (write-html* c ns node port))
                  (children node))
        (display "</" port)
        (display tagname port)
        (display #\> port)))))

(define (write-html-element-attribute attr ns port)
  (cond ((not (magic-name? (attribute-name attr)))
         (write-attribute* (attribute-serialized-name (attribute-name attr) ns)
                           (attribute-value attr)
                           port))
        ((memq (car attr) '(*namespaces* *NAMESPACES*)) ;; treat these as if they're in the xmlns namespace
         (write-namespaces (cdr attr) port)))) ;; all other magic attributes (which may include, e.g., *parent*), are ignored
(define (attribute-serialized-name qname ns)
  (let-values (((ns-name local-name) (qname-parts qname)))
    (if (not ns-name) (symbol->string local-name)
      (let ((ns-url (or (ns-name->ns-url (string->symbol ns-name) ns) ns-name)))
        (cond
         ((equal? ns-url "http://www.w3.org/XML/1998/namespace")
          (string-join (list "xml:" (symbol->string local-name)) ""))
         ((equal? ns-url "http://www.w3.org/1999/xlink")
          (string-join (list "xlink:" (symbol->string local-name)) ""))
         (else qname))))))
(define (write-namespaces namespaces port)
  (for-each
   (lambda (ns-binding)
    (let ((ns-name (car ns-binding)) 
          (ns-url (cadr ns-binding)))
      (write-attribute* (string-join (list "xmlns:" (symbol->string ns-name)) "") ns-url port)))
   namespaces))
(define (write-attribute* serialized-name value port)
  (display #\space port)
  (display serialized-name port)
  (display #\= port)
  (display #\x22 port)
  (write-html-escaped-string value port #t)
  (display #\x22 port))

(define (write-html-magic begin contents end port)
  (display begin port)
  (display contents port)
  (display end port))

(define (write-html-comment node port)
  (write-html-magic "<!--" (cadr node) "-->" port))

(define (write-html-pi node port)
  (write-html-magic "<?" (cadr node) #\> port))

(define (write-html-doctype node port)
  (write-html-magic "<!DOCTYPE " (cadr node) #\> port))

(define (write-html-escaped-string node port is-attribute?)
  ;; does not exactly implement the HTML algorithm (which is written in terms of replacements in a string), but gives equivalent results
  (string-for-each (lambda (ch)
    (case ch
      ((#\&) (display "&amp;" port))
      ((#\xA0) (display "&nbsp;" port))
      ((#\x22)
        (if is-attribute?
          (display "&quot;" port)
          (display ch port)))
      ((#\<)
        (if (not is-attribute?)
          (display "&lt;" port)
          (display ch port)))
      ((#\>)
        (if (not is-attribute?)
          (display "&gt;" port)
          (display ch port)))
      (else (display ch port)))) node))
