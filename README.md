# `html-writer`

`html-writer` implements in Scheme the [standardized HTML serialization algorithm][whatwg] (as of 19 February 2021), as adapted for SXML usage. The output, mutatis mutandis, of calling the functions documented below on an SXML tree should be equivalent to getting the JavaScript `outerHTML` property on the DOM object that SXML tree would represent in the browser.

[whatwg]: https://html.spec.whatwg.org/multipage/parsing.html#serialising-html-fragments

The implementation has no dependencies apart from R7RS-small and one of the current string utility library SRFIs: 13, 130, or 152. `html-writer` works with any of these three and has been tested on Gerbil Scheme (13) and Chibi Scheme (130).

## Procedures

### `(write-html node [port])`

Writes an HTML serialization of the SXML tree `node` to the character output port, `port`, or to the current output port if `port` is not given. Raises an error in many, but far from all cases, when `node` is not quite a valid SXML tree.

### `(html->string node)`

Returns an HTML serialization of the SXML tree `node` as a string. Uses `write-html` internally.

### `(write-html* node ns parent-node port)`

Internal version of `write-html`, exposed for flexibility when necessary. All arguments are mandatory. `ns` is an association list mapping namespace identifiers (symbols) to URLs (strings). `parent-node` is understood to be the parent of `node`, since some nodes in an HTML tree serialize differently depending on what their parent is (most notably, text nodes inside of `script`, `style`, and some other tags).

## Caveats

`html-writer` implements the WHATWG standardized algorithm for serializing HTML, with adaptations for SXML. This means that SXML trees produced from parsing real-world HTML from a [compliant parser][parse] of any kind should round-trip back into correct HTML. A strictly compliant parser, for instance, will put all elements in the HTML tree into a namespace (usually `http://www.w3.org/1999/xhtml`), and `html-writer` as a compliant serializer will remove mentions of them from the output.

[parse]: https://html.spec.whatwg.org/multipage/parsing.html

This could produce rather odd-seeming results in edge cases, depending on what you’re expecting, though. In particular, it’s not possible to generate HTML source in which elements from the HTML, SVG, or MathML namespaces appear with prefixed QNames, nor in which attributes in the XML or XLink namespaces appear with any other prefixes than `xml:` or `xlink:` respectively. Browsers may not understand elements and attributes written like that anyway, I’m not sure. If you really want to write them like that for some reason, you should probably look serving at a fully XML serialization of HTML (with the `application/xhtml+xml` MIME type which enables XML, not HTML, parsing).

The following SXML forms are treated as ‘magic’, where `s` is a single string:

* `(*top* ...)` simply serializes its children, one after the other. May have attributes, but these are not written in any way — see the comment on `*namespaces*`.
* `(*doctype* s)` represents a DOCTYPE. When authoring, this should be `(*doctype* "html")` as the first child of a `*top*` node.
* `(*pi* s)` represents a processing instruction. When authoring, this should probably never be used.
* `(*comment* s)` represents an HTML comment `<!--...-->`.
* `(*namespaces* alist)` as an attribute of an element will write appropriate `xmlns:` attributes for each of the namespace bindings in `alist`, and remember those bindings when serializing descendent elements. When used as an attribute of a `*top*` node, the serializer will keep track of the namespace binding, but will *not* write the binding names anywhere in the document.

(‘Authoring’ refers to using this library for the usual expected purpose of creating new HTML documents, rather than parsing, (possibly) modifying, and re-serializing an existing document.)

The names of all magic nodes can be written in all-upper or all-lower-case.

## Todo

* Work out how to distribute this sensibly. (Snow? Akku?)
* Maybe parameterize the list of HTML empty elements and rawtext elements, as Lassi Kortela suggested, so that developers can add new HTML elements which follow those parsing rules themselves. But I’m not sure how useful that will be in guaranteeing correctness long-term, since the serialization algorithm is changing in other ways too.
* The WHATWG algorithm prescribes special treatment of an HTML element’s ‘`is` value’. I look a cursory look at what that was and it looked like a deep rabbit-hole and something of no relevance in an SXML context. But if I’m wrong, it might need a special `*is*` magic attribute for elements or something, I’m not sure. Investigate.
* Think of a sane solution to the problem with namespaces, mentioned in the comment at the top of `write-html-element`.
* Consider a `*rawtext*` magic node, in case someone wants to stringly embed pre-serialized HTML (e.g. from an external Markdown processor or something) into their otherwise `html-writer`-generated document.
* A templating engine around this, a Sinatra- or Flask-like web framework, a standardized (in a SRFI) Scheme web application gateway comparable to Rack or WSGI, getting a Scheme HTTP or FastCGI server to talk to that gateway …

## Authoress

[Daphne Preston-Kendal](http://dpk.io/)
